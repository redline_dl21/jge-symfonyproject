<?php

namespace  JgeBundle\Form;

use JgeBundle\Entity\Sponsor;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SponsorType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameSponsor', null, [
                'label' => 'Nom du Partenaire',
                'attr' => ['class' => 'form-control']
            ])

            ->add('imageSponsor',  Type\FileType::class, [
                'attr' => ['class' => 'form-control-file'],
                'data_class' => null,
                'label' => 'image : '])

            ->add('webSiteSponsor', null, [
                'label' => 'Site du Partenaire',
                'attr' => ['class' => 'form-control'],
                'required' => false,
            ]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Sponsor::class,
        ));
    }
}