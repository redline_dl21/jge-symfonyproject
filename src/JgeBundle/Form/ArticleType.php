<?php

namespace JgeBundle\Form;

use JgeBundle\Entity\CategoryArticle;
use JgeBundle\Entity\Admin;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;



class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('titleArticle', Type\TextType::class, [
            'attr' => ['class' => 'form-control'],
            'label' => 'Titre de l\'article : '])

            ->add('contentArticle', Type\TextareaType::class, [
                'attr' => ['class' => 'form-control, tinymce'],
                'label' => 'Contenu de l\'article : '])

            ->add('imageHeadArticle', Type\FileType::class, [
                'required' => false,
                'attr' => ['class' => 'form-control-file'],
                'label' => 'image : '])

            ->add('categoryArticle', EntityType::class, [
                'attr' => ['class' => 'form-control'],
                'class'        => CategoryArticle::class,
                'choice_label' => 'titleCategoryArticle'])


            ->add('admin', EntityType::class, [
                'attr' => ['class' => 'form-control'],
                'class' => Admin::class,
                'choice_label' => 'FirstNameAdmin']);


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JgeBundle\Entity\Article'
        ));
    }

}
