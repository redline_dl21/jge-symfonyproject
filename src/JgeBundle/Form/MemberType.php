<?php

namespace JgeBundle\Form;

use JgeBundle\Entity\Member;
use JgeBundle\Entity\Player;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;

class MemberType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder


            ->add('nameMember', Type\TextType::class, [
                'label' => 'Nom',
                'attr' => ['class' => 'form-control'],
            ])
            ->add('firstNameMember', Type\TextType::class, [
                'label' => 'Prénom',
                'attr' => ['class' => 'form-control'],
            ])
            ->add('addressMember', Type\TextType::class, [
                'label' => 'Adresse',
                'attr' => ['class' => 'form-control'],
            ])

            ->add('cityMember', Type\TextType::class, [
                'label' => 'Ville',
                'attr' => ['class' => 'form-control'],
            ])

            ->add('pCMember', Type\NumberType::class, [
                'label' => 'Code Postal',
                'attr' => ['class' => 'form-control'],
            ])

            ->add('phoneMember', Type\TextType::class, [
                'label' => 'Numéro de téléphone',
                'attr' => ['class' => 'form-control'],
            ])

            ->add('emailMember', Type\EmailType::class, [
                'label' => 'Email',
                'attr' => ['class' => 'form-control'],
            ])

          ->add('dobMember', Type\BirthdayType::class, [
                'label' => 'Date de naissance',
              'attr' => ['class' => 'form-control'],
            ])

           ->add('numMember', Type\TextType::class, [
               'label' => 'Numéro de licence',
               'attr' => ['class' => 'form-control'],
            ])

            ->add('codeAccessMember', Type\TextType::class, [
                'label' => 'Code d\'accès',
                'attr' => ['class' => 'form-control'],
            ])

            ->add('photoMember', null, [
                'label' => 'Photo du membre',
                'attr' => ['class' => 'form-control-file'],
                'required' => false,
            ])

            ->add('sexMember', Type\TextType::class, [
               'label' => 'Sexe du membre',
                'attr' => ['class' => 'form-control'],
            ])
           ->add('refereeMember', null, [
                'label' => 'Arbitre',
               'attr' => ['class' => 'form-control'],
            ])
            ->add('coachMember', null, [
                'label' => 'Entraineur',
                'attr' => ['class' => 'form-control'],
            ])
           ->add('managerMember', null, [
               'label' => 'Responsable',
               'attr' => ['class' => 'form-control'],
            ])

            ->add('player', PlayerType::class, [
                'label' => 'Joueur',
                'attr' => ['class' => 'form-control'],
            ])

            ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JgeBundle\Entity\Member',
        ));
    }
}
