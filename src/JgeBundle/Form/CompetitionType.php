<?php

namespace JgeBundle\Form;

use Doctrine\Common\Collections\ArrayCollection;
use JgeBundle\Entity\Competition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use JgeBundle\Entity\Event;


class CompetitionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameCompetition', null, [
                'label' => 'Nom de la compétition',
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'JgeBundle\Entity\Competition',
        ]);
    }
}
