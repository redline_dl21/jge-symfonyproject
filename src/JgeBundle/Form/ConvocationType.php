<?php

namespace JgeBundle\Form;

use JgeBundle\Entity\Event;
use JgeBundle\Entity\Member;
use JgeBundle\JgeBundle;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;

class ConvocationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('locationConvocation', Type\TextType::class, [
            'label' => 'Lieu de RDV',
            'attr' => ['class' => 'form-control']
        ])
            ->add('hStartConvocation', Type\DateTimeType::class, [
                'label' => "Heure du début de match",
                'attr' => ['class' => 'form-control']
            ])
            ->add('hEndConvocation', Type\DateTimeType::class, [
                'label' => "Heure de fin de match",
                'attr' => ['class' => 'form-control']
            ])
            ->add('commentConvocation', Type\TextareaType::class, [
                'label' => "Commentaire",
                'attr' => ['class' => 'form-control']
            ])
            ->add('hConvocation', Type\DateTimeType::class, [
                'label' => "Heure du départ",
                'attr' => ['class' => 'form-control']
            ])
            ->add('event', EntityType::class, [
                'class' => Event::class,
                'choice_label' => 'id',
                'label' => "Match concerné ",
                'attr' => ['class' => 'form-control']
            ])
            ->add('members', EntityType::class, [
                'class' => Member::class,
                'choice_label' => 'nameMember',
                'multiple' => true,
                'expanded' => true,
                'attr' => ['class' => 'form-control']
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JgeBundle\Entity\Convocation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'jgebundle_convocation';
    }


}
