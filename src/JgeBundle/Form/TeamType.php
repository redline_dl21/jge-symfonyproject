<?php

namespace JgeBundle\Form;

use JgeBundle\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;

class TeamType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameTeam',Type\TextType::class, [
                'label' => 'Nom',
                'attr' => ['class' => 'form-control'],
                ])

            ->add('category', EntityType::class, [
                'class' => Category::class,
                'label'=> 'Nom de la catégorie',
                'choice_label' => 'NameCategory',
                'multiple' => false,
                'expanded' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => 'JgeBundle\Entity\Team',
            ]
        );
    }
}
