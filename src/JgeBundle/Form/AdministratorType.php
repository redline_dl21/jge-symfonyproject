<?php

namespace JgeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Email;

class AdministratorType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameAdmin', null, [
                'label' => "Nom",
                'attr' => ['class' => 'form-control'],
            ])
            ->add('firstNameAdmin', null, [
                'label' => "Prénom",
                'attr' => ['class' => 'form-control'],
            ])
            ->add('emailAdmin', EmailType::class, [
                'label' => "Email",
                'attr' => ['class' => 'form-control'],
            ])
            ->add('pseudoAdmin', null, [
                'label' => "Pseudo",
                'attr' => ['class' => 'form-control'],
            ])
            ->add('passwordAdmin', null, [
                'label' => "Mot de passe",
                'attr' => ['class' => 'form-control'],
            ])
            ->add('accessAdmin', ChoiceType::class, [
                'label' => "Accès administrateur",
                'attr' => ['class' => 'form-control'],
                'choices'  => array(
                    true => 'Oui',
                    false => 'Non',
                ),
            ])
            ->add('accessResponsableAdmin', ChoiceType::class, [
                'label' => "Accès Responsable",
                'attr' => ['class' => 'form-control'],
                'choices'  => array(
                    true => 'Oui',
                    false => 'Non',
                ),
            ])
            ->add('accessRedactorAdmin', ChoiceType::class, [
                'label' => "Accès Rédacteur",
                'attr' => ['class' => 'form-control'],
                'choices'  => array(
                    true => 'Oui',
                    false => 'Non',
                ),
            ]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JgeBundle\Entity\Admin',
        ));
    }
}