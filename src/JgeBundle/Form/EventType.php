<?php

namespace JgeBundle\Form;

use JgeBundle\Entity\Competition;
use JgeBundle\Entity\Convocation;
use JgeBundle\Entity\Team;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;

class EventType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateEvent', Type\DateType::class, [
                'label' => "Date du match",
                'attr' => ['class' => 'form-control']
            ])
            ->add('teamAdvEvent', null, [
                'label' => "Nom de l'équipe adverse",
                'attr' => ['class' => 'form-control']
            ])
            ->add('locationEvent', null, [
                'label' => "Lieu du match",
                'attr' => ['class' => 'form-control']
            ])
            ->add('scoreAdvEvent', Type\NumberType::class, [
                'label' => "Score de l'équipe adverse",
                'attr' => ['class' => 'form-control'],
                'required' => false
            ])
            ->add('scoreDomEvent', Type\NumberType::class, [
                'label' => "Score de l'équipe domicile",
                'attr' => ['class' => 'form-control'],
                'required' => false
            ])
            ->add('team', EntityType::class, [
                'class' => Team::class,
                'choice_label' => 'nameTeam',
                'attr' => ['class' => 'form-control'],
            ])
            ->add('competition', EntityType::class, [
                'class' => Competition::class,
                'choice_label' => 'nameCompetition',
                'attr' => ['class' => 'form-control']
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JgeBundle\Entity\Event'
        ));
    }
}