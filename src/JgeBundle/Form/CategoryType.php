<?php

namespace JgeBundle\Form;

use Doctrine\Common\Collections\ArrayCollection;
use JgeBundle\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use JgeBundle\Entity\SubCategory;
use JgeBundle\Entity\Team;


class CategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameCategory', null, [
                'label' => 'Nom de la catégorie',
            ])

            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'JgeBundle\Entity\Category',
        ]);
    }
}
