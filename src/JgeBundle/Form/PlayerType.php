<?php

namespace JgeBundle\Form;


use JgeBundle\Entity\Member;
use JgeBundle\Entity\Player;
use JgeBundle\Entity\SubCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;

class PlayerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sizePlayer', null, [
                'label' => 'Taille',
            ])
            ->add('shoesSizePlayer', Type\IntegerType::class, [
                'label'     => 'Pointure',
                'precision' => 2,
            ])
            ->add('subCategory', EntityType::class, [
                'class'=>SubCategory::class,
                'label' => 'Sous-Catégorie',
                'choice_label'=>'nameSubCategory',
                //           'multiple'=> true,
                'expanded'=> true,
            ]);

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Player::class);
    }
}