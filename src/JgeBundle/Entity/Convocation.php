<?php

namespace JgeBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Convocation
 */
class Convocation
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $locationConvocation;

    /**
     * @var \DateTime
     */
    private $hStartConvocation;

    /**
     * @var string
     */
    private $hEndConvocation;

    /**
     * @var string
     */
    private $commentConvocation;

    /**
     * @var \DateTime
     */
    private $dateCreatedConvocation;

    /**
     * @var \DateTime
     */
    private $hConvocation;

    /**
     * @var \DateTime
     */
    private $dateUpdatedConvocation;

    /**
     * @var Event
     */
    private $event;

    /**
     * @var ArrayCollection
     */
    private $members;

    /**
     * Convocation constructor.
     */
    function __construct()
    {
        $this->members = new ArrayCollection();
    }

    /**
     * Get members
     *
     * @return ArrayCollection
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Add member
     *
     * @param Member $member
     * return $this
     */
    public function addMember(Member $member)
    {
        if(!$this->members->contains($member)){
            $this->members->add($member);
            $member->addConvocation($this);
        }
        return $this;
    }

    /**
     * Remove member
     *
     * @param Member $member
     * return $this
     */
    public function removeMember(Member $member)
    {
        if(!$this->members->contains($member)){
            $this->members->removeElement($member);
            $member->removeConvocation($this);
        }
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locationConvocation
     *
     * @param string $locationConvocation
     * @return Convocation
     */
    public function setLocationConvocation($locationConvocation)
    {
        $this->locationConvocation = $locationConvocation;

        return $this;
    }

    /**
     * Get locationConvocation
     *
     * @return string 
     */
    public function getLocationConvocation()
    {
        return $this->locationConvocation;
    }

    /**
     * Set hStartConvocation
     *
     * @param \DateTime $hStartConvocation
     * @return Convocation
     */
    public function setHStartConvocation($hStartConvocation)
    {
        $this->hStartConvocation = $hStartConvocation;

        return $this;
    }

    /**
     * Get hStartConvocation
     *
     * @return \DateTime 
     */
    public function getHStartConvocation()
    {
        return $this->hStartConvocation;
    }

    /**
     * Set hEndConvocation
     *
     * @param string $hEndConvocation
     * @return Convocation
     */
    public function setHEndConvocation($hEndConvocation)
    {
        $this->hEndConvocation = $hEndConvocation;

        return $this;
    }

    /**
     * Get hEndConvocation
     *
     * @return string 
     */
    public function getHEndConvocation()
    {
        return $this->hEndConvocation;
    }

    /**
     * Set commentConvocation
     *
     * @param string $commentConvocation
     * @return Convocation
     */
    public function setCommentConvocation($commentConvocation)
    {
        $this->commentConvocation = $commentConvocation;

        return $this;
    }

    /**
     * Get commentConvocation
     *
     * @return string 
     */
    public function getCommentConvocation()
    {
        return $this->commentConvocation;
    }

    /**
     * Set dateCreatedConvocation
     *
     * @param \DateTime $dateCreatedConvocation
     * @return Convocation
     */
    public function setDateCreatedConvocation($dateCreatedConvocation)
    {
        $this->dateCreatedConvocation = $dateCreatedConvocation;

        return $this;
    }

    /**
     * Get dateCreatedConvocation
     *
     * @return \DateTime 
     */
    public function getDateCreatedConvocation()
    {
        return $this->dateCreatedConvocation;
    }

    /**
     * Set hConvocation
     *
     * @param \DateTime $hConvocation
     * @return Convocation
     */
    public function setHConvocation($hConvocation)
    {
        $this->hConvocation = $hConvocation;

        return $this;
    }

    /**
     * Get hConvocation
     *
     * @return \DateTime 
     */
    public function getHConvocation()
    {
        return $this->hConvocation;
    }

    /**
     * Set dateUpdatedConvocation
     *
     * @param \DateTime $dateUpdatedConvocation
     * @return Convocation
     */
    public function setDateUpdatedConvocation($dateUpdatedConvocation)
    {
        $this->dateUpdatedConvocation = $dateUpdatedConvocation;

        return $this;
    }

    /**
     * Get dateUpdatedConvocation
     *
     * @return \DateTime 
     */
    public function getDateUpdatedConvocation()
    {
        return $this->dateUpdatedConvocation;
    }

    /**
     * @return event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param event $event
     */
    public function setEvent(Event $event)
    {
        $this->event = $event;
        if ($event->getConvocation() !== $this) $event->setConvocation($this);
    }
}
