<?php

namespace JgeBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CategoryArticle
 */
class CategoryArticle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $titleCategoryArticle;

    /**
     * @var ArrayCollection
     */
    private $articles;

    public function __construct()
    {
        $this->articles = new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titleCategoryArticle
     *
     * @param string $titleCategoryArticle
     * @return CategoryArticle
     */
    public function setTitleCategoryArticle($titleCategoryArticle)
    {
        $this->titleCategoryArticle = $titleCategoryArticle;

        return $this;
    }

    /**
     * Get titleCategoryArticle
     *
     * @return string 
     */
    public function getTitleCategoryArticle()
    {
        return $this->titleCategoryArticle;
    }

    /**
     * @return ArrayCollection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Adds the article.
     * @param Article $article
     * @return $this
     */
    public function addArticle(Article $article)
    {
        if (!$this->articles->contains($article)) {
            $this->articles->add($article);
            $article->setCategoryArticle($this);
        }

        return $this;
    }

    /**
     * Removes the article.
     * @param Article $article
     * @return $this
     */
    public function removeArticle(Article $article)
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            $article->setCategoryArticle(null);
        }
        return $this;
    }
}
