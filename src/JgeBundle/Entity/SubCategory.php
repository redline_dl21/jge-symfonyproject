<?php

namespace JgeBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SubCategory
 */
class SubCategory
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nameSubCategory;

    /**
     * @var string
     */
    private $imageSubCategory;

    /**
     * @var ArrayCollection
     */
    private $players;

    /**
     * @var Category
     */
    private $category;

    public function __construct()
    {
        $this->players= new arrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameSubCategory
     *
     * @param string $nameSubCategory
     * @return SubCategory
     */
    public function setNameSubCategory($nameSubCategory)
    {
        $this->nameSubCategory = $nameSubCategory;

        return $this;
    }

    /**
     * Get nameSubCategory
     *
     * @return string 
     */
    public function getNameSubCategory()
    {
        return $this->nameSubCategory;
    }

    /**
     * Set imageSubCategory
     *
     * @param string $imageSubCategory
     * @return SubCategory
     */
    public function setImageSubCategory($imageSubCategory)
    {
        $this->imageSubCategory = $imageSubCategory;

        return $this;
    }

    /**
     * Get imageSubCategory
     *
     * @return string 
     */
    public function getImageSubCategory()
    {
        return $this->imageSubCategory;
    }

    /**
     * @return category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return ArrayCollection
     */
    public function getPlayers()
    {
        return $this->players;
    }
    /**
     * Add a player
     */
    public function addPlayer(Player $player)
    {
        if (!$this->players->contains($player)) {
            $this->players->add($player);
            $player->setSubCategory($this);
        }
        return $this;
    }

    /**
     * Remove a player
     */
    public function removePlayer(Player $player)
    {
        if ($this->players->contains($player)) {
            $this->players->removeElement($player);
            $player->setSubCategory(null);
        }
        return $this;
    }
}
