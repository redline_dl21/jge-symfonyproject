<?php

namespace JgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Player
 */
class Player
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $sizePlayer;

    /**
     * @var int
     */
    private $shoesSizePlayer;

    /**
     * @var Member
     */
    private $member;

    /**
     * @var SubCategory
     */
    private $subCategory;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sizePlayer
     *
     * @param string $sizePlayer
     * @return Player
     */
    public function setSizePlayer($sizePlayer)
    {
        $this->sizePlayer = $sizePlayer;

        return $this;
    }

    /**
     * Get sizePlayer
     *
     * @return string 
     */
    public function getSizePlayer()
    {
        return $this->sizePlayer;
    }

    /**
     * Set shoesSizePlayer
     *
     * @param integer $shoesSizePlayer
     * @return Player
     */
    public function setShoesSizePlayer($shoesSizePlayer)
    {
        $this->shoesSizePlayer = $shoesSizePlayer;

        return $this;
    }

    /**
     * Get shoesSizePlayer
     *
     * @return integer 
     */
    public function getShoesSizePlayer()
    {
        return $this->shoesSizePlayer;
    }

    /**
     * @return Member
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * @param Member $member
     */
    public function setMember(Member $member)
    {
        $this->member = $member;
    }

    /**
     * @return SubCategory
     */
    public function getSubCategory()
    {
        return $this->subCategory;
    }

    /**
     * @param SubCategory $subCategory
     */
    public function setSubCategory(SubCategory $subCategory)
    {
        $this->subCategory = $subCategory;
    }
}
