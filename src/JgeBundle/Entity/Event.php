<?php

namespace JgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 */
class Event
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateEvent;

    /**
     * @var string
     */
    private $teamAdvEvent;

    /**
     * @var string
     */
    private $locationEvent;

    /**
     * @var int
     */
    private $scoreAdvEvent;

    /**
     * @var int
     */
    private $scoreDomEvent;

    /**
     * @var Convocation
     */
    private $convocation;

    /**
     * @var Team
     */
    private $team;

    /**
     * @var Competition
     */
    private $competition;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateEvent
     *
     * @param \DateTime $dateEvent
     * @return Event
     */
    public function setDateEvent($dateEvent)
    {
        $this->dateEvent = $dateEvent;

        return $this;
    }

    /**
     * Get dateEvent
     *
     * @return \DateTime 
     */
    public function getDateEvent()
    {
        return $this->dateEvent;
    }

    /**
     * Set teamAdvEvent
     *
     * @param string $teamAdvEvent
     * @return Event
     */
    public function setTeamAdvEvent($teamAdvEvent)
    {
        $this->teamAdvEvent = $teamAdvEvent;

        return $this;
    }

    /**
     * Get teamAdvEvent
     *
     * @return string 
     */
    public function getTeamAdvEvent()
    {
        return $this->teamAdvEvent;
    }

    /**
     * Set locationEvent
     *
     * @param string $locationEvent
     * @return Event
     */
    public function setLocationEvent($locationEvent)
    {
        $this->locationEvent = $locationEvent;

        return $this;
    }

    /**
     * Get locationEvent
     *
     * @return string 
     */
    public function getLocationEvent()
    {
        return $this->locationEvent;
    }

    /**
     * Set scoreAdvEvent
     *
     * @param integer $scoreAdvEvent
     * @return Event
     */
    public function setScoreAdvEvent($scoreAdvEvent)
    {
        $this->scoreAdvEvent = $scoreAdvEvent;

        return $this;
    }

    /**
     * Get scoreAdvEvent
     *
     * @return integer 
     */
    public function getScoreAdvEvent()
    {
        return $this->scoreAdvEvent;
    }

    /**
     * Set scoreDomEvent
     *
     * @param integer $scoreDomEvent
     * @return Event
     */
    public function setScoreDomEvent($scoreDomEvent)
    {
        $this->scoreDomEvent = $scoreDomEvent;

        return $this;
    }

    /**
     * Get scoreDomEvent
     *
     * @return integer 
     */
    public function getScoreDomEvent()
    {
        return $this->scoreDomEvent;
    }

    /**
     * @return Convocation
     */
    public function getConvocation()
    {
        return $this->convocation;
    }

    /**
     * @param Convocation $convocation
     */
    public function setConvocation(Convocation $convocation)
    {
        $this->convocation = $convocation;
    }

    /**
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param Team $team
     */
    public function setTeam(Team $team)
    {
        $this->team = $team;
    }

    /**
     * @return Competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * @param Competition $competition
     */
    public function setCompetition(Competition $competition)
    {
        $this->competition = $competition;
    }
}
