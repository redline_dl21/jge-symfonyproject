<?php

namespace JgeBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Competition
 */
class Competition
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nameCompetition;
    /**
     * @var arrayCollection
     */
    private $events;

    public function __construct()
    {
        $this->events=new arrayCollection();
    }

    /**
     * add event
     * @param Event $event
     * @return $this
     */
    public function addEvent($event) {
        if(!$this->events->contains($event)){
            $this->events->add($event);
            $event->setCompetition($this);
        }
        return $this;
    }

    /**
     * remove event
     * @param Event $event
     * @return $this
     */
    public function removeEvent($event) {
        if($this->events->contains($event)){
            $this->events->add($event);
            $event->setCompetition(null);
        }
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameCompetition
     *
     * @param string $nameCompetition
     * @return Competition
     */
    public function setNameCompetition($nameCompetition)
    {
        $this->nameCompetition = $nameCompetition;

        return $this;
    }

    /**
     * Get nameCompetition
     *
     * @return string 
     */
    public function getNameCompetition()
    {
        return $this->nameCompetition;
    }

    /**
     * @return ArrayCollection
     */
    public function getEvents()
    {
        return $this->events;
    }
}
