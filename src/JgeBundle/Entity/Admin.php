<?php

namespace JgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Admin
 */
class Admin
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nameAdmin;

    /**
     * @var string
     */
    private $firstNameAdmin;

    /**
     * @var string
     */
    private $emailAdmin;

    /**
     * @var string
     */
    private $pseudoAdmin;

    /**
     * @var string
     */
    private $passwordAdmin;

    /**
     * @var bool
     */
    private $accessAdmin;

    /**
     * @var bool
     */
    private $accessResponsableAdmin;

    /**
     * @var bool
     */
    private $accessRedactorAdmin;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameAdmin
     *
     * @param string $nameAdmin
     * @return Admin
     */
    public function setNameAdmin($nameAdmin)
    {
        $this->nameAdmin = $nameAdmin;

        return $this;
    }

    /**
     * Get nameAdmin
     *
     * @return string 
     */
    public function getNameAdmin()
    {
        return $this->nameAdmin;
    }

    /**
     * Set firstNameAdmin
     *
     * @param string $firstNameAdmin
     * @return Admin
     */
    public function setFirstNameAdmin($firstNameAdmin)
    {
        $this->firstNameAdmin = $firstNameAdmin;

        return $this;
    }

    /**
     * Get firstNameAdmin
     *
     * @return string 
     */
    public function getFirstNameAdmin()
    {
        return $this->firstNameAdmin;
    }

    /**
     * Set emailAdmin
     *
     * @param string $emailAdmin
     * @return Admin
     */
    public function setEmailAdmin($emailAdmin)
    {
        $this->emailAdmin = $emailAdmin;

        return $this;
    }

    /**
     * Get emailAdmin
     *
     * @return string 
     */
    public function getEmailAdmin()
    {
        return $this->emailAdmin;
    }

    /**
     * Set pseudoAdmin
     *
     * @param string $pseudoAdmin
     * @return Admin
     */
    public function setPseudoAdmin($pseudoAdmin)
    {
        $this->pseudoAdmin = $pseudoAdmin;

        return $this;
    }

    /**
     * Get pseudoAdmin
     *
     * @return string 
     */
    public function getPseudoAdmin()
    {
        return $this->pseudoAdmin;
    }

    /**
     * Set passwordAdmin
     *
     * @param string $passwordAdmin
     * @return Admin
     */
    public function setPasswordAdmin($passwordAdmin)
    {
        $this->passwordAdmin = $passwordAdmin;

        return $this;
    }

    /**
     * Get passwordAdmin
     *
     * @return string 
     */
    public function getPasswordAdmin()
    {
        return $this->passwordAdmin;
    }

    /**
     * Set accessAdmin
     *
     * @param boolean $accessAdmin
     * @return Admin
     */
    public function setAccessAdmin($accessAdmin)
    {
        $this->accessAdmin = $accessAdmin;

        return $this;
    }

    /**
     * Get accessAdmin
     *
     * @return boolean 
     */
    public function getAccessAdmin()
    {
        return $this->accessAdmin;
    }

    /**
     * Set accessResponsableAdmin
     *
     * @param boolean $accessResponsableAdmin
     * @return Admin
     */
    public function setAccessResponsableAdmin($accessResponsableAdmin)
    {
        $this->accessResponsableAdmin = $accessResponsableAdmin;

        return $this;
    }

    /**
     * Get accessResponsableAdmin
     *
     * @return boolean 
     */
    public function getAccessResponsableAdmin()
    {
        return $this->accessResponsableAdmin;
    }

    /**
     * Set accessRedactorAdmin
     *
     * @param boolean $accessRedactorAdmin
     * @return Admin
     */
    public function setAccessRedactorAdmin($accessRedactorAdmin)
    {
        $this->accessRedactorAdmin = $accessRedactorAdmin;

        return $this;
    }

    /**
     * Get accessRedactorAdmin
     *
     * @return boolean 
     */
    public function getAccessRedactorAdmin()
    {
        return $this->accessRedactorAdmin;
    }
}
