<?php

namespace JgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Article
 */
class Article
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $titleArticle;

    /**
     * @var string
     */
    private $contentArticle;

    /**
     * @var \DateTime
     */
    private $dateCreatedArticle;

    /**
     * @ORM\Column(type="string")
     */
    private $imageHeadArticle;

    /**
     * @var categoryArticle
     */
    private $categoryArticle;
    /**
     * @var admin
     */
    private $admin;

    public function __construct()
    {
        $this->dateCreatedArticle= new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titleArticle
     *
     * @param string $titleArticle
     * @return Article
     */
    public function setTitleArticle($titleArticle)
    {
        $this->titleArticle = $titleArticle;

        return $this;
    }

    /**
     * Get titleArticle
     *
     * @return string 
     */
    public function getTitleArticle()
    {
        return $this->titleArticle;
    }

    /**
     * Set contentArticle
     *
     * @param string $contentArticle
     * @return Article
     */
    public function setContentArticle($contentArticle)
    {
        $this->contentArticle = $contentArticle;

        return $this;
    }

    /**
     * Get contentArticle
     *
     * @return string 
     */
    public function getContentArticle()
    {
        return $this->contentArticle;
    }

    /**
     * Set dateCreatedArticle
     *
     * @param \DateTime $dateCreatedArticle
     * @return Article
     */
    public function setDateCreatedArticle($dateCreatedArticle)
    {
        $this->dateCreatedArticle = $dateCreatedArticle;

        return $this;
    }

    /**
     * Get dateCreatedArticle
     *
     * @return \DateTime 
     */
    public function getDateCreatedArticle()
    {
        return $this->dateCreatedArticle;
    }

    /**
     * Set imageHeadArticle
     *
     * @param string $imageHeadArticle
     * @return Article
     */
    public function setImageHeadArticle($imageHeadArticle)
    {
        $this->imageHeadArticle = $imageHeadArticle;

        return $this;
    }

    /**
     * Get imageHeadArticle
     *
     * @return string 
     */
    public function getImageHeadArticle()
    {
        return $this->imageHeadArticle;
    }

    /**
     * @return categoryArticle
     */
    public function getCategoryArticle()
    {
        return $this->categoryArticle;
    }
    /**
     * @param CategoryArticle $categoryArticle
     * @return Article
     */
    public function setCategoryArticle(CategoryArticle $categoryArticle = null)
    {
        if ($categoryArticle !== $this->categoryArticle) {
            $this->categoryArticle = $categoryArticle;

            if (!$categoryArticle) {
                $categoryArticle->addArticle($this);
            }
        }

        return $this;
    }

    /**
     * @return admin
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Set admin
     *
     * @param \JgeBundle\Entity\Admin $admin
     * @return Article
     */
    public function setAdmin(Admin $admin)
    {
        $this->admin = $admin;

        return $this;
    }
}
