<?php

namespace JgeBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 */
class Team
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nameTeam;

    /**
     * @var arrayCollection
     */
    private $events;

    /**
     * @var Category
     */
    private $category;


    public function __construct()
    {
        $this->events=new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameTeam
     *
     * @param string $nameTeam
     * @return Team
     */
    public function setNameTeam($nameTeam)
    {
        $this->nameTeam = $nameTeam;

        return $this;
    }

    /**
     * Get nameTeam
     *
     * @return string 
     */
    public function getNameTeam()
    {
        return $this->nameTeam;
    }

    /**
     * @return category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return ArrayCollection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Adds the event.
     * @param Event $event
     * @return $this
     */
    public function addEventTeam(Event $event)
    {
        if(!$this->events->contains($event)) {
            $this->events->add($event);
            $event->setTeam($this);
        };
        return $this;
    }

    /**
     * Removes the event.
     * @param Event $event
     * @return $this
     */
    public function removeEventTeam(Event $event)
    {
        if($this->events->contains($event)){
            $this->events->removeElement($event);
            $event->setTeam(null);
        }
        return $this;
    }
}
