<?php

namespace JgeBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 */
class Category
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nameCategory;

    /**
     * @var arrayCollection
     */
    private $subCategories;

    /**
     * @var arrayCollection
     */
    private $teams;


    public function __construct()
    {
        $this->subCategories= new arrayCollection();
        $this->teams= new arrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getSubCategories()
    {
        return $this->subCategories;
    }

    /**
     * Adds the subCategory.
     * @param SubCategory $subCategory
     * @return $this
     */
    public function addSubCategory(SubCategory $subCategory)
    {
        if(!$this->subCategories->contains($subCategory)) {
            $this->subCategories->add($subCategory);
            $subCategory->setCategory($this);
        };
        return $this;
    }

    /**
     * Removes the subCategory.
     * @param SubCategory $subCategory
     * @return $this
     */
    public function removeSubCategory(SubCategory $subCategory) {
        if($this->subCategories->contains($subCategory)){
            $this->subCategories->removeElement($subCategory);
            $subCategory->setCategory(null);
        }
        return $this;
    }

    /**
     * Adds the team.
     * @param Team $team
     * @return $this
     */

    public function addTeam(Team $team)
    {
        if(!$this->teams->contains($team)) {
            $this->teams->add($team);
            $team->setCategory($this);
        };
        return $this;
    }

    /**
     * Removes the team.
     * @param Team $team
     * @return $this
     */
    public function removeTeam(Team $team) {
        if($this->teams->contains($team)){
            $this->teams->removeElement($team);
            $team->setCategory(null);
        }
        return $this;

    }

    /**
     * @return ArrayCollection
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameCategory
     *
     * @param string $nameCategory
     * @return Category
     */
    public function setNameCategory($nameCategory)
    {
        $this->nameCategory = $nameCategory;

        return $this;
    }

    /**
     * Get nameCategory
     *
     * @return string 
     */
    public function getNameCategory()
    {
        return $this->nameCategory;
    }
}
