<?php

namespace JgeBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Member
 */
class Member
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nameMember;

    /**
     * @var string
     */
    private $firstNameMember;

    /**
     * @var string
     */
    private $addressMember;

    /**
     * @var string
     */
    private $cityMember;

    /**
     * @var int
     */
    private $pCMember;

    /**
     * @var int
     */
    private $phoneMember;

    /**
     * @var string
     */
    private $emailMember;

    /**
     * @var \DateTime
     */
    private $dobMember;

    /**
     * @var string
     */
    private $numMember;

    /**
     * @var string
     */
    private $codeAccessMember;

    /**
     * @var string
     */
    private $photoMember;

    /**
     * @var string
     */
    private $sexMember;

    /**
     * @var bool
     */
    private $refereeMember;

    /**
     * @var bool
     */
    private $coachMember;

    /**
     * @var bool
     */
    private $managerMember;

    /**
     * @var Player
     */
    private $player;

    /**
     * @ var ArrayCollection
     */
    private $convocations;

    public function __construct()
    {
        $this->player= new Player();
        $this->convocations = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getConvocations()
    {
        return $this->convocations;
    }

    /**
     * Adds the convocation.
     * @param Convocation $convocation
     * @return $this
     */
    public function addConvocation(Convocation $convocation)
    {
        if(!$this->convocations->contains($convocation)){
            $this->convocations->add($convocation);
            $convocation->addMember($this);
        }
        return $this;
    }

    /**
     * Removes the convocation
     *
     * @param Convocation $convocation
     * @return $this
     */
    public function removeConvocation(Convocation $convocation)
    {
        if($this->convocations->contains($convocation)){
            $this->convocations->removeElement($convocation);
            $convocation->removeMember($this);
        }
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameMember
     *
     * @param string $nameMember
     * @return Member
     */
    public function setNameMember($nameMember)
    {
        $this->nameMember = $nameMember;

        return $this;
    }

    /**
     * Get nameMember
     *
     * @return string 
     */
    public function getNameMember()
    {
        return $this->nameMember;
    }

    /**
     * Set firstNameMember
     *
     * @param string $firstNameMember
     * @return Member
     */
    public function setFirstNameMember($firstNameMember)
    {
        $this->firstNameMember = $firstNameMember;

        return $this;
    }

    /**
     * Get firstNameMember
     *
     * @return string 
     */
    public function getFirstNameMember()
    {
        return $this->firstNameMember;
    }

    /**
     * Set addressMember
     *
     * @param string $addressMember
     * @return Member
     */
    public function setAddressMember($addressMember)
    {
        $this->addressMember = $addressMember;

        return $this;
    }

    /**
     * Get addressMember
     *
     * @return string 
     */
    public function getAddressMember()
    {
        return $this->addressMember;
    }

    /**
     * Set cityMember
     *
     * @param string $cityMember
     * @return Member
     */
    public function setCityMember($cityMember)
    {
        $this->cityMember = $cityMember;

        return $this;
    }

    /**
     * Get cityMember
     *
     * @return string 
     */
    public function getCityMember()
    {
        return $this->cityMember;
    }

    /**
     * Set pCMember
     *
     * @param integer $pCMember
     * @return Member
     */
    public function setPCMember($pCMember)
    {
        $this->pCMember = $pCMember;

        return $this;
    }

    /**
     * Get pCMember
     *
     * @return integer 
     */
    public function getPCMember()
    {
        return $this->pCMember;
    }

    /**
     * Set phoneMember
     *
     * @param integer $phoneMember
     * @return Member
     */
    public function setPhoneMember($phoneMember)
    {
        $this->phoneMember = $phoneMember;

        return $this;
    }

    /**
     * Get phoneMember
     *
     * @return integer 
     */
    public function getPhoneMember()
    {
        return $this->phoneMember;
    }

    /**
     * Set emailMember
     *
     * @param string $emailMember
     * @return Member
     */
    public function setEmailMember($emailMember)
    {
        $this->emailMember = $emailMember;

        return $this;
    }

    /**
     * Get emailMember
     *
     * @return string 
     */
    public function getEmailMember()
    {
        return $this->emailMember;
    }

    /**
     * Set dobMember
     *
     * @param \DateTime $dobMember
     * @return Member
     */
    public function setDobMember($dobMember)
    {
        $this->dobMember = $dobMember;

        return $this;
    }

    /**
     * Get dobMember
     *
     * @return \DateTime 
     */
    public function getDobMember()
    {
        return $this->dobMember;
    }

    /**
     * Set numMember
     *
     * @param string $numMember
     * @return Member
     */
    public function setNumMember($numMember)
    {
        $this->numMember = $numMember;

        return $this;
    }

    /**
     * Get numMember
     *
     * @return string 
     */
    public function getNumMember()
    {
        return $this->numMember;
    }

    /**
     * Set codeAccessMember
     *
     * @param string $codeAccessMember
     * @return Member
     */
    public function setCodeAccessMember($codeAccessMember)
    {
        $this->codeAccessMember = $codeAccessMember;

        return $this;
    }

    /**
     * Get codeAccessMember
     *
     * @return string 
     */
    public function getCodeAccessMember()
    {
        return $this->codeAccessMember;
    }

    /**
     * Set photoMember
     *
     * @param string $photoMember
     * @return Member
     */
    public function setPhotoMember($photoMember)
    {
        $this->photoMember = $photoMember;

        return $this;
    }

    /**
     * Get photoMember
     *
     * @return string 
     */
    public function getPhotoMember()
    {
        return $this->photoMember;
    }

    /**
     * Set sexMember
     *
     * @param string $sexMember
     * @return Member
     */
    public function setSexMember($sexMember)
    {
        $this->sexMember = $sexMember;

        return $this;
    }

    /**
     * Get sexMember
     *
     * @return string 
     */
    public function getSexMember()
    {
        return $this->sexMember;
    }

    /**
     * Set refereeMember
     *
     * @param boolean $refereeMember
     * @return Member
     */
    public function setRefereeMember($refereeMember)
    {
        $this->refereeMember = $refereeMember;

        return $this;
    }

    /**
     * Get refereeMember
     *
     * @return boolean 
     */
    public function getRefereeMember()
    {
        return $this->refereeMember;
    }

    /**
     * Set coachMember
     *
     * @param boolean $coachMember
     * @return Member
     */
    public function setCoachMember($coachMember)
    {
        $this->coachMember = $coachMember;

        return $this;
    }

    /**
     * Get coachMember
     *
     * @return boolean 
     */
    public function getCoachMember()
    {
        return $this->coachMember;
    }

    /**
     * Set managerMember
     *
     * @param boolean $managerMember
     * @return Member
     */
    public function setManagerMember($managerMember)
    {
        $this->managerMember = $managerMember;

        return $this;
    }

    /**
     * Get managerMember
     *
     * @return boolean 
     */
    public function getManagerMember()
    {
        return $this->managerMember;
    }

    /**
     * @return player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @param player $player
     */
    public function setPlayer($player)
    {
        $this->player = $player;
    }
}
