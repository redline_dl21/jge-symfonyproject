<?php

namespace JgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sponsor
 */
class Sponsor
{
    /**
     * @var int
     */
    private $id;


    /**
     * @var string
     */
    private $nameSponsor;

    /**
     * @var string
     */
    private $imageSponsor;

    /**
     * @var string
     */
    private $webSiteSponsor;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set nameSponsor
     *
     * @param string $nameSponsor
     * @return Sponsor
     */
    public function setNameSponsor($nameSponsor)
    {
        $this->nameSponsor = $nameSponsor;

        return $this;
    }

    /**
     * Get nameSponsor
     *
     * @return string 
     */
    public function getNameSponsor()
    {
        return $this->nameSponsor;
    }

    /**
     * Set imageSponsor
     *
     * @param string $imageSponsor
     * @return Sponsor
     */
    public function setImageSponsor($imageSponsor)
    {
        $this->imageSponsor = $imageSponsor;

        return $this;
    }

    /**
     * Get imageSponsor
     *
     * @return string 
     */
    public function getImageSponsor()
    {
        return $this->imageSponsor;
    }

    /**
     * Set webSiteSponsor
     *
     * @param string $webSiteSponsor
     * @return Sponsor
     */
    public function setWebSiteSponsor($webSiteSponsor)
    {
        $this->webSiteSponsor = $webSiteSponsor;

        return $this;
    }

    /**
     * Get webSiteSponsor
     *
     * @return string 
     */
    public function getWebSiteSponsor()
    {
        return $this->webSiteSponsor;
    }
}
