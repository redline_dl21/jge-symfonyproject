<?php

namespace JgeBundle\Controller\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ResultController extends Controller {

    /*
     * Displays the list of all results
     *
     */
    public function indexAction() {

        $events = $this->getDoctrine()
            ->getRepository('JgeBundle:Event')
            ->findAll();

        $category = $this->getDoctrine()
            ->getRepository('JgeBundle:Category')
            ->findAll();

        $teams = $this->getDoctrine()
            ->getRepository('JgeBundle:Team')
            ->findAll();


        return $this->render('JgeBundle:Result:result.html.twig', [
            'category' => $category,
            'teams' => $teams,
            'events' => $events
        ]);

    }

    /*
     * Displays the list of all results by team
     *
     */
    public function teamAction($id_team) {

        $team = $this->getDoctrine()
            ->getRepository('JgeBundle:Team')
            ->findOneBy(['id' => $id_team]);

        $events = $this->getDoctrine()
            ->getRepository('JgeBundle:Event')
            ->findBy(['team' => $team], ['dateEvent' => 'DESC']);

        $category = $this->getDoctrine()
            ->getRepository('JgeBundle:Category')
            ->findAll();

        $teams = $this->getDoctrine()
            ->getRepository('JgeBundle:Team')
            ->findAll();


        return $this->render('JgeBundle:Result:resultTeam.html.twig', [
            'category' => $category,
            'teams' => $teams,
            'team' => $team,
            'events' => $events
        ]);

    }
}