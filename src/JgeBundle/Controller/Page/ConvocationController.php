<?php

namespace JgeBundle\Controller\Page;

use JgeBundle\Entity\Member;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class ConvocationController extends Controller {

    /*
     * Displays the index of the convocation
     *
     */
    public function indexAction(Request $request) {

        $session = $request->getSession();

        if($session->get('member') instanceof Member) {

            $member = $session->get('member');
            $name = $member->getNameMember();
            $firstName = $member->getFirstNameMember();

            return $this->render('JgeBundle:Convocation:index.html.twig', array(
                'name' => $name,
                'firstName' => $firstName,
            ));
        }
        else
            return $this->redirectToRoute('page_convocation_login');
    }

    public function loginAction() {

        if(isset($_POST['accessCode'])) {

            $em = $this->getDoctrine()->getManager();

            $member = $em->getRepository('JgeBundle:Member')->findOneBy(
                array('codeAccessMember' => $_POST['accessCode'])
            );

            if($member != Null) {

                $session = new Session();

                $session->set('member', $member);

                return $this->redirectToRoute('page_convocation_index');
            }
            else {
                $this->addFlash('notice','Votre identifiant ou mot de passe est incorrect');
            }
        }
        return $this->render('JgeBundle:Page:login.html.twig');
    }

    public function logoutAction(Request $request) {

        $session = $request->getSession();
        $session->invalidate();
        return $this->redirectToRoute('page_home');
    }

    /*
     * Displays the convocation by category
     *
     */
    public function categoryAction() {

    }

    /*
     * Displays the convocation by team
     *
     */
    public function teamAction() {

    }
}