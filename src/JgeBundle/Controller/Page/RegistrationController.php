<?php

namespace JgeBundle\Controller\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RegistrationController extends Controller {

    /*
     * Displays the information page to register to the JGE club
     *
     */
    public function indexAction() {

    }

    /*
     * Displays the information page about the summer activities at the JGE club
     *
     */
    public function summerActivitiesAction() {

    }
}