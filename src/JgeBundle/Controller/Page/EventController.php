<?php

namespace JgeBundle\Controller\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EventController extends Controller {

    /*
     * Displays the list of all matches
     *
     */
    public function indexAction() {

    }

    /*
     * Displays the list of all matches by team
     *
     */
    public function teamAction() {

    }
}