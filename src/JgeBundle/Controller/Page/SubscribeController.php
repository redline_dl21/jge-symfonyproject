<?php

namespace JgeBundle\Controller\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SubscribeController extends Controller
{
    public function clubAction(){

        $category = $this->getDoctrine()
            ->getRepository('JgeBundle:CategoryArticle')
            ->findBy(['titleCategoryArticle' => 'inscription-club']);

        $articles = $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findBy(['categoryArticle' => $category], ['dateCreatedArticle' => 'DESC' ], 1);

        return $this->render('JgeBundle:Subscribe:club.html.twig',[
            'articles' => $articles
        ]);

    }

    public function stageAction(){

        $category = $this->getDoctrine()
            ->getRepository('JgeBundle:CategoryArticle')
            ->findBy(['titleCategoryArticle' => 'inscription-stage']);

        $articles = $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findBy(['categoryArticle' => $category], ['dateCreatedArticle' => 'DESC' ], 1);


        return $this->render('JgeBundle:Subscribe:stage.html.twig', [
            'articles'=> $articles
        ]);

    }

}