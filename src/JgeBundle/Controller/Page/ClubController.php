<?php

namespace JgeBundle\Controller\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ClubController extends Controller {



    /*
     * Displays the presentation page of all teams
     * /!\ The page is called "équipes" but it actually displays the categories /!\
     *
     */
    public function teamAction() {

        $category = $this->getDoctrine()
            ->getRepository('JgeBundle:Category')
            ->findAll();

        $teams = $this->getDoctrine()
            ->getRepository('JgeBundle:Team')
            ->findAll();

        $categoryArticle = $this->getDoctrine()
            ->getRepository('JgeBundle:CategoryArticle')
            ->findBy(['titleCategoryArticle' => 'club-équipes']);

        $articles = $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findBy(['categoryArticle' => $categoryArticle],['dateCreatedArticle' => 'DESC'],1);

        return $this->render('JgeBundle:Club:teams.html.twig', [
            'articles' => $articles,
            'category' => $category,
            'teams' => $teams,
        ]);

    }
    public function teamdetailAction() {

        $category = $this->getDoctrine()
            ->getRepository('JgeBundle:Category')
            ->findAll();

        $teams = $this->getDoctrine()
            ->getRepository('JgeBundle:Team')
            ->findAll();

        $categoryArticle = $this->getDoctrine()
            ->getRepository('JgeBundle:CategoryArticle')
            ->findBy(['titleCategoryArticle' => 'club-équipes']);

        $articles = $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findBy(['categoryArticle' => $categoryArticle],['dateCreatedArticle' => 'DESC'],1);

        return $this->render('JgeBundle:Club:team-details.html.twig', [
            'articles' => $articles,
            'category' => $category,
            'teams' => $teams
        ]);

    }

    /*
     * Displays the staff organization chart
     *
     */
    public function officeAction() {

        $categoryArticle = $this->getDoctrine()
            ->getRepository('JgeBundle:CategoryArticle')
            ->findBy(['titleCategoryArticle' => 'club-bureau']);

        $articles = $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findBy(['categoryArticle' => $categoryArticle],['dateCreatedArticle' => 'DESC'],3);

        return $this->render('JgeBundle:Club:office.html.twig', [
            'articles' => $articles
        ]);

    }

    /*
     * Displays the information page about the stadium
     *
     */
    public function stadiumAction() {

        $categoryArticle = $this->getDoctrine()
            ->getRepository('JgeBundle:CategoryArticle')
            ->findBy(['titleCategoryArticle' => 'club-stade']);

        $articles = $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findBy(['categoryArticle' => $categoryArticle],['dateCreatedArticle' => 'DESC'],3);

        return $this->render('JgeBundle:Club:stadium.html.twig', [
            'articles' => $articles
        ]);

    }

    /*
     * Displays a list of all the records of the club
     *
     */
    public function aboutAction() {

        $categoryArticle = $this->getDoctrine()
            ->getRepository('JgeBundle:CategoryArticle')
            ->findBy(['titleCategoryArticle' => 'club-propos']);

        $articles = $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findBy(['categoryArticle' => $categoryArticle],['dateCreatedArticle' => 'DESC'], 3);

        $category = $this->getDoctrine()
            ->getRepository('JgeBundle:CategoryArticle')
            ->findBy(['titleCategoryArticle' => 'club-propos-fiche']);

        $articleTech = $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findBy(['categoryArticle' => $category],['dateCreatedArticle' => 'DESC'], 1);

        return $this->render('JgeBundle:Club:about.html.twig', [
            'articles' => $articles,
            'articleTech' => $articleTech
        ]);
    }

    public function staffAction(){


        return $this->render('JgeBundle:Club:staff.html.twig', [


        ]);
    }

    public function refereeAction(){

        $referees = $this->getDoctrine()
            ->getRepository('JgeBundle:Member')
            ->findBy(['refereeMember' => true]);

        return $this->render('JgeBundle:Club:referees.html.twig', [
            'referees' => $referees
        ]);
    }
}