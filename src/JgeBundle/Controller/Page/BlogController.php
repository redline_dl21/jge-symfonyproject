<?php

namespace JgeBundle\Controller\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BlogController extends Controller {

    /*
     * Displays the list of all the latest articles
     *
     */
    public function indexAction() {
        $category = $this->getDoctrine()
            ->getRepository('JgeBundle:CategoryArticle')
            ->findBy(['titleCategoryArticle' => ['blog','actualités','événements']]);

        $articlesdated = $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findAll();

        $articles = $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findBy(['categoryArticle' => $category], ['dateCreatedArticle' => 'DESC' ], 10);

        return $this->render('JgeBundle:Blog:index.html.twig', [
            'articles' => $articles,
            'category' => $category,
            'articlesdated' => $articlesdated
        ]);

    }

    /*
     * Displays the list of all the latest articles by category
     *
     */
    public function articleAction($article_id) {

        $article = $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findOneBy(['id' => $article_id]);

        return $this->render('JgeBundle:Blog:article.html.twig',[
            'article' => $article,
        ]);

    }

    public function categoryAction($categoryArticle_id){

        $categoryArticle = $this->getDoctrine()
            ->getRepository('JgeBundle:CategoryArticle')
            ->findOneBy(['id' => $categoryArticle_id]);

//        $articles= $this->getDoctrine()
//            ->getRepository('JgeBundle:Article')
//            ->findOneBy([
//                'id' => $categoryArticle_id,
//                'id' => $article_id]);

        return $this->render('JgeBundle:Blog:categoryArticle.html.twig',[
            'categoryArticle'=> $categoryArticle,
            'articles' => $categoryArticle->getArticles(),
        ]);
    }
}