<?php

namespace JgeBundle\Controller\Page;
use jgeBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller {

    /*
     * Displays the home page of the JGE website
     *
     */
    public function homeAction()
    {

        $latestPost = $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findBy([], ['dateCreatedArticle' => 'DESC'], 3);

        $actualCategory = $this->getDoctrine()
            ->getRepository('JgeBundle:CategoryArticle')
            ->findBy(['titleCategoryArticle'=>'Actualité']);

        $lastArticle= $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findBy(['categoryArticle'=> $actualCategory],['dateCreatedArticle' => 'DESC'],1);

        $latestEvents = $this->getDoctrine()
            ->getRepository('JgeBundle:Event')
            ->findBy([], ['dateEvent' => 'DESC'], 4);

        return $this->render('JgeBundle:Page:home.html.twig', [
            'articles' => $latestPost,
            'latestEvents' => $latestEvents,
            'lastArticle'=> $lastArticle

        ]);
      }

    /*
     * Displays the login page to access the convocations
     *
     */
    public function loginAction() {

        return $this->render('JgeBundle:Page:login.html.twig');

    }

    /*
     * Displays the store page
     *
     */
    public function storeAction() {

        $category = $this->getDoctrine()
            ->getRepository('JgeBundle:CategoryArticle')
            ->findBy(['titleCategoryArticle' => 'boutique']);

        $articles = $this->getDoctrine()
            ->getRepository('JgeBundle:Article')
            ->findBy(['categoryArticle' => $category], ['dateCreatedArticle' => 'DESC' ]);

        return $this->render('JgeBundle:Page:store.html.twig', [
            'articles' => $articles
        ]);

    }

    /*
     * Displays the contact page
     *
     */
    public function ContactAction(Request $request) {


        $form = $this->createFormBuilder( [
            'action' => $this->generateUrl('page_contact'),
            'method' => 'POST',
            'attr' => [
                'class' => 'form-group',
            ]
        ])
            ->add('email','email',[
                'attr' => ['class' =>'form-control'],
                'label' => 'Adresse email : ',

            ])
            ->add('subject', 'text', [
                'attr' => ['class' =>'form-control','rows' => 29],
                'label' => 'Objet : ',

            ])
            ->add('message', 'textarea', [
                'attr' => ['class' =>'form-control'],
                'label' => 'Message : '
            ])
            ->add('Envoyer', 'submit', [
                'attr' => ['class' =>'btn btn-success']

            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $message = \Swift_Message::newInstance()
                ->setSubject($data['subject'])
                ->setFrom($data['email'])
                ->setTo('jeremie.hardouin@laposte.net')
                ->setBody( 'text/html');

            $sent = $this->get('mailer')->send($message);
            if (0 < $sent) {
                $this->addFlash('success', 'Email envoyé !');
            }

            return $this->redirectToRoute('page_contact');

        }

        return $this->render('JgeBundle:Page:contact.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function PartnersAction(){

        $partners = $this->getDoctrine()
            ->getRepository('JgeBundle:Sponsor')
            ->findAll();

        return $this->render('JgeBundle:Page:sponsor.html.twig',[
            'sponsors' => $partners
        ]);

    }

    /*
     * Displays the site-map page
     *
     */
    public function siteMapAction() {

    }

    /*
     * Displays the legal-information page
     *
     */
    public function legalInformationAction() {

    }


}