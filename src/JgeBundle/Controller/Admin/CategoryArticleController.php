<?php

namespace JgeBundle\Controller\Admin;

use JgeBundle\Entity\CategoryArticle;
use JgeBundle\Entity\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class CategoryArticleController extends Controller {

    /**
     * Lists all categoryArticle entities
     *
     */
    public function indexAction(Request $request) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $em = $this->getDoctrine()->getManager();

            $categoriesArticle = $em->getRepository('JgeBundle:CategoryArticle')->findAll();

            return $this->render('JgeBundle:Admin/CategoryArticle:index.html.twig', array(
                'categoriesArticle' => $categoriesArticle,
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Create a new categoryArticle entity
     *
     */
    public function newAction(Request $request) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesRedacteur = $admin->getAccessRedactorAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesRedacteur==true)) {

            $categoryArticle = new CategoryArticle();
            $form = $this
                ->createForm('JgeBundle\Form\CategoryArticleType', $categoryArticle)
                ->add('Sauvegarder', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-xs btn-success',
                    ]
                ]);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($categoryArticle);
                $em->flush();

                return $this->redirectToRoute('admin_categoryArticle_index');
            }

            return $this->render('JgeBundle:Admin/CategoryArticle:new.html.twig', array(
                'categoryArticle' => $categoryArticle,
                'form' => $form->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Finds and displays a categoryArticle entity
     *
     */
    public function showAction(Request $request, CategoryArticle $categoryArticle) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $deleteForm = $this->createDeleteForm($categoryArticle);

            return $this->render('JgeBundle:Admin/CategoryArticle:show.html.twig', array(
                'categoryArticle' => $categoryArticle,
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Displays a form to edit an existing categoryArticle entity
     *
     */
    public function editAction(Request $request, CategoryArticle $categoryArticle) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesRedacteur = $admin->getAccessRedactorAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesRedacteur==true)) {

            $deleteForm = $this->createDeleteForm($categoryArticle);
            $editForm = $this
                ->createForm('JgeBundle\Form\CategoryArticleType', $categoryArticle)
                ->add('save', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-success',
                    ]
                ]);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('admin_categoryArticle_show', array('id' => $categoryArticle->getId()));
            }

            return $this->render('JgeBundle:Admin/CategoryArticle:edit.html.twig', array(
                'category' => $categoryArticle,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Deletes a categoryArticle entity
     *
     */
    public function deleteAction(Request $request, CategoryArticle $categoryArticle) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesRedacteur = $admin->getAccessRedactorAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesRedacteur==true)) {

            $form = $this->createDeleteForm($categoryArticle);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($categoryArticle);
                $em->flush();
            }
            return $this->redirectToRoute('admin_categoryArticle_index');
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param CategoryArticle $categoryArticle The category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CategoryArticle $categoryArticle)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_categoryArticle_delete', array('id' => $categoryArticle->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}