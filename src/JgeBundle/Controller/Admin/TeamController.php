<?php

namespace JgeBundle\Controller\Admin;

use JgeBundle\Entity\Team;
use JgeBundle\Entity\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TeamController extends Controller {

    /**
     * Lists all team entities
     *
     */
    public function indexAction(Request $request) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $em = $this->getDoctrine()->getManager();

            $teams = $em->getRepository('JgeBundle:Team')->findAll();

            return $this->render('JgeBundle:Admin/Team:index.html.twig', array(
                'teams' => $teams,
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Create a new team entity
     *
     */
    public function newAction(Request $request){

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $team = new Team();
            $form = $this
                ->createForm('JgeBundle\Form\TeamType', $team)
                ->add('Sauvegarder', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-xs btn-success',
                    ]
                ]);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($team);
                $em->flush();

                // Message flash
                $this->addFlash('success', 'Equipe ajoutée.');

                // Redirection
                return $this->redirectToRoute('admin_team_index');
            }

            return $this->render('JgeBundle:Admin/Team:new.html.twig', array(
                'team' => $team,
                'form' => $form->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Finds and displays a team entity
     *
     */
    public function showAction(Team $team, Request $request) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $deleteForm = $this->createDeleteForm($team);

            return $this->render('JgeBundle:Admin/Team:show.html.twig', array(
                'team' => $team,
                'category' => $team->getCategory(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Displays a form to edit an existing team entity
     *
     */
    public function editAction(Request $request, Team $team) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $deleteForm = $this->createDeleteForm($team);
            $editForm = $this
                ->createForm('JgeBundle\Form\TeamType', $team)
                ->add('save', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-success',
                    ]
                ]);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('admin_team_show', array('id' => $team->getId()));
            }

            return $this->render('JgeBundle:Admin/Team:edit.html.twig', array(
                'team' => $team,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Deletes a team entity
     *
     */
    public function deleteAction(Request $request, Team $team) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $form = $this->createDeleteForm($team);

            $form->handleRequest($request);
            $events = $team . getEvents();
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                var_dump($team . getEvents());

//            if (!$team.getEventsTeam()){
//                $em->remove($team);
//                $em->flush();
//            }
//            else return $this->addFlash('alert','Des matchs sont enregistrés pour cette équipe,
//            vous ne pouvez pas supprimer cette équipe! Vous pouvez seulement la modifier ou créer une autre équipe');

            }

            return $this->redirectToRoute('admin_team_index');
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Creates a form to delete a team entity.
     *
     * @param Team $team The team entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    public function createDeleteForm($team)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_team_delete', array('id' => $team->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}