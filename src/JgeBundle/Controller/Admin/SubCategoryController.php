<?php

namespace JgeBundle\Controller\Admin;

use JgeBundle\Entity\SubCategory;
use JgeBundle\Entity\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class SubCategoryController extends Controller {

    /**
     * Lists all subCategory entities
     *
     */
    public function indexAction(Request $request) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $em = $this->getDoctrine()->getManager();

            $subCategories = $em->getRepository('JgeBundle:SubCategory')->findAll();

            return $this->render('JgeBundle:Admin/SubCategory:index.html.twig', array(
                'subCategories' => $subCategories,
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Create a new subCategory entity
     *
     */
    public function newAction(Request $request){

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $subCategory = new SubCategory();
            $form = $this
                ->createForm('JgeBundle\Form\SubCategoryType', $subCategory)
                ->add('save', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-success',
                    ]
                ]);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($subCategory);
                $em->flush();

                // Message flash
                $this->addFlash('success', 'Sous cat�gorie ajout�e.');

                // Redirection
                return $this->redirectToRoute('admin_subCategory_index');
            }

            return $this->render('JgeBundle:Admin/SubCategory:new.html.twig', array(
                'subCategory' => $subCategory,
                'form' => $form->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Finds and displays a subCategory entity
     *
     */
    public function showAction(SubCategory $subCategory, Request $request) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $deleteForm = $this->createDeleteForm($subCategory);

            return $this->render('JgeBundle:Admin/SubCategory:show.html.twig', array(
                'subCategory' => $subCategory,
                'category' => $subCategory->getCategory(),
                'players' => $subCategory->getPlayers(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Displays a form to edit an existing subCategory entity
     *
     */
    public function editAction(Request $request, SubCategory $subCategory) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $deleteForm = $this->createDeleteForm($subCategory);
            $editForm = $this
                ->createForm('JgeBundle\Form\CategoryType', $subCategory)
                ->add('save', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-success',
                    ]
                ]);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('admin_subCategory_show', array('id' => $subCategory->getId()));
            }

            return $this->render('JgeBundle:Admin/Category:edit.html.twig', array(
                'subCategory' => $subCategory,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Deletes a subCategory entity
     *
     */
    public function deleteAction(Request $request, SubCategory $subCategory) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $form = $this->createDeleteForm($subCategory);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($subCategory);
                $em->flush();
            }

            return $this->redirectToRoute('admin_subCategory_index');
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Creates a form to delete a subCategory entity.
     *
     * @param SubCategory $subCategory The subCategory entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    public function createDeleteForm(SubCategory $subCategory)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_subCategory_delete', array('id' => $subCategory->getId())))
            ->setMethod('DELETE')
            ->getForm();

    }
}