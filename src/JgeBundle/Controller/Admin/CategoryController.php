<?php

namespace JgeBundle\Controller\Admin;

use JgeBundle\Entity\Category;
use JgeBundle\Entity\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller {

    /**
     * Lists all category entities
     *
     */
    public function indexAction(Request $request) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $em = $this->getDoctrine()->getManager();

            $categories = $em->getRepository('JgeBundle:Category')->findAll();

            return $this->render('JgeBundle:Admin/Category:index.html.twig', array(
                'categories' => $categories,
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }


    /**
     * Create a new category entity
     *
     */
    public function newAction(Request $request){

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $category = new Category();
            $form = $this
                ->createForm('JgeBundle\Form\CategoryType', $category)
                ->add('save', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-success',
                    ]
                ]);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();

                // Message flash
                $this->addFlash('success', 'Categorie ajoutée.');

                // Redirection
                return $this->redirectToRoute('admin_category_index');
            }

            return $this->render('JgeBundle:Admin/Category:new.html.twig', array(
                'category' => $category,
                'form' => $form->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Finds and displays a category entity
     *
     */
    public function showAction(Category $category, Request $request) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $deleteForm = $this->createDeleteForm($category);

            return $this->render('JgeBundle:Admin/Category:show.html.twig', array(
                'category' => $category,
                'subCategories' => $category->getSubCategories(),
                'teams' => $category->getTeams(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Displays a form to edit an existing category entity
     *
     */
    public function editAction(Request $request, Category $category) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $deleteForm = $this->createDeleteForm($category);
            $editForm = $this
                ->createForm('JgeBundle\Form\CategoryType', $category)
                ->add('save', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-success',
                    ]
                ]);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('admin_category_show', array('id' => $category->getId()));
            }

            return $this->render('JgeBundle:Admin/Category:edit.html.twig', array(
                'category' => $category,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Deletes a category entity
     *
     */
    public function deleteAction(Request $request, Category $category) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $form = $this->createDeleteForm($category);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($category);
                $em->flush();
            }

            return $this->redirectToRoute('admin_category_index');
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param Category $category The category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    public function createDeleteForm($category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_category_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm();

    }
}