<?php

namespace JgeBundle\Controller\Admin;

use JgeBundle\Entity\Member;
use JgeBundle\Entity\Admin;
use JgeBundle\Entity\Player;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class MemberController extends Controller {

    /**
     * Lists all member entities
     *
     */
    public function indexAction(Request $request) {
        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $em = $this->getDoctrine()->getManager();

            $members = $em->getRepository('JgeBundle:Member')
                ->findAll();

            return $this->render('JgeBundle:Admin/Member:index.html.twig', array(
                'members' => $members,
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Create a new member entity
     *
     */
    public function newAction(Request $request) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $member = new Member();
            $form = $this
                ->createForm('JgeBundle\Form\MemberType', $member)
                ->add('Sauvegarder', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-xs btn-success',
                    ]
                ]);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($member);
                $em->flush($member);

                return $this->redirectToRoute('admin_member_show', array('id' => $member->getId()));
            }

            return $this->render('JgeBundle:Admin/Member:new.html.twig', array(
                'member' => $member,
                'form' => $form->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }



    /**
     * Finds and displays a member entity
     *
     */
    public function showAction(Member $member, Request $request) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $deleteForm = $this->createDeleteForm($member);

            return $this->render('JgeBundle:Admin/Member:show.html.twig', array(
                'member' => $member,
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Displays a form to edit an existing member entity
     *
     */
    public function editAction(Request $request, Member $member) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $deleteForm = $this->createDeleteForm($member);
            $editForm = $this
                ->createForm('JgeBundle\Form\MemberType', $member)
                ->add('save', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-primary',
                    ]
                ]);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('admin_member_edit', array('id' => $member->getId()));
            }

            return $this->render('JgeBundle:Admin/Member:edit.html.twig', array(
                'member' => $member,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Deletes a member entity
     *
     */
    public function deleteAction(Request $request, Member $member) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $form = $this->createDeleteForm($member);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($member);
                $em->flush();
            }

            return $this->redirectToRoute('admin_member_index');
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    private function createDeleteForm(Member $member)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_member_delete', array('id' => $member->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }


}