<?php

namespace JgeBundle\Controller\Admin;

use JgeBundle\Entity\Article;
use JgeBundle\Entity\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;

class ArticleController extends Controller {

    /**
     * Lists all article entities
     *
     */
    public function indexAction(Request $request) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $articleList = $this->getDoctrine()
                ->getManager()
                ->getRepository('JgeBundle:Article')
                ->findAll();

            return $this->render('JgeBundle:Admin/Article:articles.html.twig', [
                'articles' => $articleList,
            ]);
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');

    }

    /**
     * Create a new article entity
     *
     */
    public function newAction(Request $request) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesRedacteur = $admin->getAccessRedactorAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesRedacteur==true)) {

            $article = new Article();

            $form = $this->createForm('JgeBundle\Form\ArticleType', $article)
                ->add('sauvegarder', new SubmitType(), [
                    'attr' => ['class' => 'btn btn-success btn-sm']
                ]);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                $file = $article->getImageHeadArticle();

                if (!empty($file)) {
                    $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                    $dir = $this->getParameter('kernel.root_dir') . '/../web/uploads/pictures';
                    $file->move($dir, $fileName);
                    $article->setImageHeadArticle($fileName);
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($article);
                $em->flush();


                return $this->redirectToRoute('admin_article_show', ['id' => $article->getId()]);
            }

            return $this->render('JgeBundle:Admin/Article:new.html.twig', [
                'article' => $article,
                'form' => $form->createView(),
            ]);
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Finds and displays an article entity
     *
     */
    public function showAction(Request $request, Article $article) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $deleteForm = $this->createDeleteForm($article);

            return $this->render('JgeBundle:Admin/Article:show.html.twig', array(
                'article' => $article,
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Displays a form to edit an existing article entity
     *
     */
    public function editAction(Request $request, Article $article) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesRedacteur = $admin->getAccessRedactorAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesRedacteur==true)) {
            $oldImg = $article->getImageHeadArticle();
            $dirOldImg = $this->getParameter('kernel.root_dir') .
                '/../web/uploads/pictures/' . $article->getImageHeadArticle();

            $article->setImageHeadArticle(new File($dirOldImg));

            $deleteForm = $this->createDeleteForm($article);
            $editForm = $this
                ->createForm('JgeBundle\Form\ArticleType', $article)
                ->add('sauvegarder', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-primary',
                    ],
                ]);

            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {

                $newImg = $article->getImageHeadArticle();

                if (empty($newImg)) {
                    $article->setImageHeadArticle($oldImg);
                } else {
                    $fileName = md5(uniqid()) . '.' . $newImg->guessExtension();
                    $dir = $this->getParameter('kernel.root_dir') . '/../web/uploads/pictures';
                    $newImg->move($dir, $fileName);
                    $article->setImageHeadArticle($fileName);
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($article);
                $em->flush();

                return $this->redirectToRoute('admin_article_show', array('id' => $article->getId()));
            }

            return $this->render('JgeBundle:Admin/Article:edit.html.twig', array(
                'article' => $article,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Deletes an article entity
     *
     */
    public function deleteAction(Request $request, Article $article) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesRedacteur = $admin->getAccessRedactorAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesRedacteur==true)) {

            $form = $this->createDeleteForm($article);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($article);
                $em->flush();
            }

            return $this->redirectToRoute('admin_article_index');
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Creates a form to delete a article entity.
     *
     * @param Article $article The article entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Article $article)
    {
        return $this->createFormBuilder([''])
            ->setAction($this->generateUrl('admin_article_delete', array('id' => $article->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }


}