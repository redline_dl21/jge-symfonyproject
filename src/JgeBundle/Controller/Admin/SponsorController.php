<?php

namespace JgeBundle\Controller\Admin;

use JgeBundle\Entity\Sponsor;
use JgeBundle\Entity\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Sponsor controller.
 *
 */
class SponsorController extends Controller
{
    /**
     * Lists all sponsor entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sponsors = $em->getRepository('JgeBundle:Sponsor')->findAll();

        return $this->render('JgeBundle:Admin/Sponsor:index.html.twig', array(
            'sponsors' => $sponsors,
        ));
    }

    /**
     * Creates a new sponsor entity.
     *
     */
    public function newAction(Request $request)
    {
        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && ($accesAdmin==true)) {

            $sponsor = new Sponsor();

            $form = $this->createForm('JgeBundle\Form\SponsorType', $sponsor)
                ->add('sauvegarder', new SubmitType(), [
                    'attr' => ['class' => 'btn btn-success btn-sm']
                ]);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                $file = $sponsor->getImageSponsor();

                if (!empty($file)) {
                    $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                    $dir = $this->getParameter('kernel.root_dir') . '/../web/uploads/pictures/logo/';
                    $file->move($dir, $fileName);
                    $sponsor->setImageSponsor($fileName);
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($sponsor);
                $em->flush();



                return $this->redirectToRoute('admin_sponsor_show', ['id' => $sponsor->getId()]);
            }

            return $this->render('JgeBundle:Admin/Sponsor:new.html.twig', [
                'sponsor' => $sponsor,
                'form' => $form->createView(),
            ]);
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Finds and displays a sponsor entity.
     *
     */
    public function showAction(Sponsor $sponsor)
    {

        $deleteForm = $this->createDeleteForm($sponsor);

        return $this->render('JgeBundle:Admin/Sponsor:show.html.twig', array(
            'sponsor' => $sponsor,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing sponsor entity.
     *
     */
    public function editAction(Request $request, Sponsor $sponsor)
    {
        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && ($accesAdmin==true)) {
            $oldImg = $sponsor->getImageSponsor();
            $dirOldImg = $this->getParameter('kernel.root_dir') .
                '/../web/uploads/pictures/logo/' . $sponsor->getImageSponsor();

            $sponsor->setImageSponsor(new File($dirOldImg));

            $deleteForm = $this->createDeleteForm($sponsor);
            $editForm = $this
                ->createForm('JgeBundle\Form\SponsorType', $sponsor)
                ->add('sauvegarder', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-primary',
                    ],
                ]);

            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {

                $newImg = $sponsor->getImageSponsor();

                if (empty($newImg)) {
                    $sponsor->setImageSponsor($oldImg);
                } else {
                    $fileName = md5(uniqid()) . '.' . $newImg->guessExtension();
                    $dir = $this->getParameter('kernel.root_dir') . '/../web/uploads/pictures/logo/';
                    $newImg->move($dir, $fileName);
                    $sponsor->setImageSponsor($fileName);
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($sponsor);
                $em->flush();

                return $this->redirectToRoute('admin_sponsor_show', array('id' => $sponsor->getId()));
            }

            return $this->render('JgeBundle:Admin/Sponsor:edit.html.twig', array(
                'sponsor' => $sponsor,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Deletes a sponsor entity.
     *
     */
    public function deleteAction(Request $request, Sponsor $sponsor)
    {
        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && ($accesAdmin==true)) {

            $form = $this->createDeleteForm($sponsor);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($sponsor);
                $em->flush();
            }

            return $this->redirectToRoute('admin_sponsor_index');
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Creates a form to delete a sponsor entity.
     *
     * @param Sponsor $sponsor The sponsor entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sponsor $sponsor)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_sponsor_delete', array('id' => $sponsor->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}