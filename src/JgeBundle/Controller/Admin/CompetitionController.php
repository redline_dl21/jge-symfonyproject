<?php

namespace JgeBundle\Controller\Admin;

use JgeBundle\Entity\Competition;
use JgeBundle\Entity\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CompetitionController extends Controller {

    /**
     * Lists all competition entities
     *
     */
    public function indexAction(Request $request) {
        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $em = $this->getDoctrine()->getManager();

            $competitions = $em->getRepository('JgeBundle:Competition')->findAll();

            return $this->render('JgeBundle:Admin/Competition:index.html.twig', array(
                'competitions' => $competitions,
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Create a new competition entity
     *
     */
    public function newAction(Request $request){

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesResponsable = $admin->getAccessResponsableAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesResponsable==true)) {

            $competition = new Competition();
            $form = $this
                ->createForm('JgeBundle\Form\CompetitionType', $competition)
                ->add('save', new SubmitType(), [

                    'attr' => [
                        'class' => 'btn btn-xs btn-success',
                    ]
                ]);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($competition);
                $em->flush();

                // Message flash
                $this->addFlash('success', 'Compétition ajoutée.');

                // Redirection
                return $this->redirectToRoute('admin_competition_index');
            }

            return $this->render('JgeBundle:Admin/Competition:new.html.twig', array(
                'competition' => $competition,
                'form' => $form->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Finds and displays a competition entity
     *
     */
    public function showAction(Competition $competition, Request $request) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $deleteForm = $this->createDeleteForm($competition);

            return $this->render('JgeBundle:Admin/Competition:show.html.twig', array(
                'competition' => $competition,
                'events' => $competition->getEvents(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Displays a form to edit an existing competition entity
     *
     */
    public function editAction(Request $request, Competition $competition) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesResponsable = $admin->getAccessResponsableAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesResponsable==true)) {

            $deleteForm = $this->createDeleteForm($competition);
            $editForm = $this
                ->createForm('JgeBundle\Form\CompetitionType', $competition)
                ->add('save', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-success',
                    ]
                ]);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('admin_competition_show', array('id' => $competition->getId()));
            }

            return $this->render('JgeBundle:Admin/Competition:edit.html.twig', array(
                'competition' => $competition,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Deletes a competition entity
     *
     */
    public function deleteAction(Request $request, Competition $competition) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesResponsable = $admin->getAccessResponsableAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesResponsable==true)) {

            $form = $this->createDeleteForm($competition);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($competition);
                $em->flush();
            }
            return $this->redirectToRoute('admin_competition_index');
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Creates a form to delete a competition entity.
     *
     * @param Competition $competition The competition entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    public function createDeleteForm($competition)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_competition_delete', array('id' => $competition->getId())))
            ->setMethod('DELETE')
            ->getForm();

    }
}