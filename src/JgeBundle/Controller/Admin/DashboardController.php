<?php

namespace JgeBundle\Controller\Admin;
use JgeBundle\Entity\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

class DashboardController extends Controller {

    /**
     * Show the index page of the administration
     *
     */
    public function indexAction(Request $request) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $admin = $session->get('admin');
            $name = $admin->getNameAdmin();
            $firstName = $admin->getFirstNameAdmin();

            return $this->render('JgeBundle:Admin:home.html.twig', array(
                'name' => $name,
                'firstName' => $firstName,
            ));
        }
        else
            return $this->redirectToRoute('admin_login');
    }

    /*
     * Display the form and check if the values are correct to create a new session
     *
     */
    public function loginAction() {

        if(isset($_POST['username']) && ($_POST['password'])) {

            $em = $this->getDoctrine()->getManager();

            $administrator = $em->getRepository('JgeBundle:Admin')->findOneBy(
                array('pseudoAdmin' => $_POST['username'], 'passwordAdmin' => $_POST['password'])
            );

            if($administrator != Null) {

                $session = new Session();

                $session->set('admin', $administrator);

                return $this->redirectToRoute('admin_dashboard');
            }
            else {
                $this->addFlash('notice','Votre identifiant ou mot de passe est incorrect');
                //$this->render('JgeBundle:Admin:login.html.twig');
            }
        }
        return $this->render('JgeBundle:Admin:login.html.twig');
    }

    /*
     * Destroy a session
     *
     */
    public function logoutAction(Request $request)
    {
        $session = $request->getSession();
        $session->invalidate();
        return $this->redirectToRoute('page_home');
    }
}