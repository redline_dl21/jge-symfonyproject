<?php

namespace JgeBundle\Controller\Admin;
use JgeBundle\Entity\Admin;
use JgeBundle\Entity\Convocation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class ConvocationController extends Controller {

    /**
     * Lists all convocation entities
     *
     */
    public function indexAction(Request $request) {
        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {
            $em = $this->getDoctrine()->getManager();
            $convocations = $em->getRepository('JgeBundle:Convocation')->findAll();
            return $this->render('JgeBundle:Admin/Convocation:index.html.twig', [
                'convocations' => $convocations,
            ]);
            }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Create a new convocation entity
     *
     */
    public function newAction(Request $request) {
        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesResponsable = $admin->getAccessResponsableAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesResponsable==true)) {

            $convocation = new Convocation();
            $form = $this->createForm('JgeBundle\Form\ConvocationType', $convocation)
                ->add('save', new SubmitType(), [
                    'attr' => [
                    'class' => 'btn btn-sm btn-success',
                    ]
                    ]);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                if (!$convocation->getDateCreatedConvocation()) $convocation->setDateCreatedConvocation(new \DateTime());
                $convocation->setDateUpdatedConvocation(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($convocation);
                $em->flush();

                // Message flash
                $this->addFlash('success', 'Convocation créée.');

                // Redirection
                return $this->redirectToRoute('admin_convocation_index');
            }

            return $this->render('JgeBundle:Admin/Convocation:new.html.twig', array(
                'convocation' => $convocation,
                'form' => $form->createView(),
            ));
            }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }
    /**
     * Finds and displays a convocation entity
     *
     */
    public function showAction(Convocation $convocation, Request $request) {

    $session = $request->getSession();

    if($session->get('admin') instanceof Admin) {

        $deleteForm = $this->createDeleteForm($request, $convocation);

            return $this->render('JgeBundle:Admin/Convocation:show.html.twig', array(
                'convocation' => $convocation,
                'event' => $convocation->getEvent(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Displays a form to edit an existing convocation entity
     *
     */
    public function editAction(Convocation $convocation, Request $request) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesResponsable = $admin->getAccessResponsableAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesResponsable==true)) {

            $deleteForm = $this->createDeleteForm($request, $convocation);
            $editForm = $this
                ->createForm('JgeBundle\Form\ConvocationType', $convocation)
                ->add('save', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-success',
                    ]
                ]);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('admin_convocation_show', array('id' => $convocation->getId()));
            }

            return $this->render('JgeBundle:Admin/Convocation:edit.html.twig', array(
                'convocation' => $convocation,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Deletes a convocation entity
     *
     */
    public function deleteAction(Request $request, Convocation $convocation) {
        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesResponsable = $admin->getAccessResponsableAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesResponsable==true)) {

            $form = $this->createDeleteForm($request, $convocation);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($convocation);
                $em->flush();
            }
            return $this->redirectToRoute('admin_convocation_index');
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    public function createDeleteForm(Request $request, Convocation $convocation)
    {
        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesResponsable = $admin->getAccessResponsableAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesResponsable==true)) {

            return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_convocation_delete', array('id' => $convocation->getId())))
            ->setMethod('DELETE')
            ->getForm();
        }
    else return $this->render('JgeBundle:Admin:error403.html.twig');
    }
}