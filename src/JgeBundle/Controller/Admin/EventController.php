<?php

namespace JgeBundle\Controller\Admin;

use JgeBundle\Entity\Event;
use JgeBundle\Entity\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class EventController extends Controller {

    /**
     * Lists all event entities
     *
     */
    public function indexAction(Request $request) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $em = $this->getDoctrine()->getManager();

            $events = $em->getRepository('JgeBundle:Event')->findAll();

            return $this->render('JgeBundle:Admin/Event:index.html.twig', array(
                'events' => $events,
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Create a new event entity
     *
     */
    public function newAction(Request $request) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesResponsable = $admin->getAccessResponsableAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesResponsable==true)) {

            $event = new Event();
            $form = $this
                ->createForm('JgeBundle\Form\EventType', $event)
                ->add('save', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-success',
                    ]
                ]);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($event);
                $em->flush();

                return $this->redirectToRoute('admin_event_index');
            }

            return $this->render('JgeBundle:Admin/Event:new.html.twig', array(
                'event' => $event,
                'form' => $form->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Finds and displays an event entity
     *
     */
    public function showAction(Request $request, Event $event) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $deleteForm = $this->createDeleteForm($event);

            return $this->render('JgeBundle:Admin/Event:show.html.twig', array(
                'event' => $event,
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Displays a form to edit an existing event entity
     *
     */
    public function editAction(Request $request, Event $event) {

        $session = $request->getSession();

        if($session->get('admin') instanceof Admin) {

            $deleteForm = $this->createDeleteForm($event);
            $editForm = $this
                ->createForm('JgeBundle\Form\EventType', $event)
                ->add('save', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-success',
                    ]
                ]);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($event);
                $em->flush();

                return $this->redirectToRoute('admin_event_show', array('id' => $event->getId()));
            }

            return $this->render('JgeBundle:Admin/Event:edit.html.twig', array(
                'event' => $event,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Deletes an event entity
     *
     */
    public function deleteAction(Request $request, Event $event) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();
        $accesResponsable = $admin->getAccessResponsableAdmin();

        if($admin instanceof Admin && ($accesAdmin==true || $accesResponsable==true)) {

            $form = $this->createDeleteForm($event);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($event);
                $em->flush();
            }

            return $this->redirectToRoute('admin_event_index');
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param Event $event The category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Event $event)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_event_delete', array('id' => $event->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}