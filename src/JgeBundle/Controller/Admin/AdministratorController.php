<?php

namespace JgeBundle\Controller\Admin;

use JgeBundle\Entity\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class AdministratorController extends Controller {

    /**
     * Lists all administrator entities
     *
     */
    public function indexAction(Request $request) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $em = $this->getDoctrine()->getManager();

            $administrators = $em->getRepository('JgeBundle:Admin')->findAll();

            return $this->render('JgeBundle:Admin/Administrator:index.html.twig', array(
                'administrators' => $administrators,
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Create a new administrator entity
     *
     */
    public function newAction(Request $request) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $administrator = new Admin();
            $form = $this
                ->createForm('JgeBundle\Form\AdministratorType', $administrator)
                ->add('save', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-success',
                    ]
                ]);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($administrator);
                $em->flush();

                return $this->redirectToRoute('admin_administrator_index');
            }

            return $this->render('JgeBundle:Admin/Administrator:new.html.twig', array(
                'administrator' => $administrator,
                'form' => $form->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Finds and displays an administrator entity
     *
     */
    public function showAction(Request $request, Admin $administrator) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $deleteForm = $this->createDeleteForm($administrator);

            return $this->render('JgeBundle:Admin/Administrator:show.html.twig', array(
                'administrator' => $administrator,
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Displays a form to edit an existing administrator entity
     *
     */
    public function editAction(Request $request, Admin $administrator) {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if($admin instanceof Admin && $accesAdmin==true) {

            $deleteForm = $this->createDeleteForm($administrator);
            $editForm = $this
                ->createForm('JgeBundle\Form\AdministratorType', $administrator)
                ->add('save', new SubmitType(), [
                    'attr' => [
                        'class' => 'btn btn-sm btn-success',
                    ]
                ]);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($administrator);
                $em->flush();

                return $this->redirectToRoute('admin_administrator_show', array('id' => $administrator->getId()));
            }

            return $this->render('JgeBundle:Admin/Administrator:edit.html.twig', array(
                'administrator' => $administrator,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Deletes an administrator entity
     *
     */
    public function deleteAction(Request $request, Admin $administrator)
    {

        $session = $request->getSession();
        $admin = $session->get('admin');
        $accesAdmin = $admin->getAccessAdmin();

        if ($admin instanceof Admin && $accesAdmin == true) {

            $form = $this->createDeleteForm($administrator);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($administrator);
                $em->flush();
            }
            return $this->redirectToRoute('admin_administrator_index');
        }
        else return $this->render('JgeBundle:Admin:error403.html.twig');
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param Admin $administrator The category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Admin $administrator)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_administrator_delete', array('id' => $administrator->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}